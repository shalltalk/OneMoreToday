var Quote = require("../models/quote");
var Comment = require("../models/comment");

// all the middleare goes here
var middlewareObj = {};

middlewareObj.checkQuoteOwnership = function(req, res, next) {
 if(req.isAuthenticated()){
        Quote.findById(req.params.id, function(err, foundQuote){
           if(err){
               req.flash("error", "Quote not found");
               res.redirect("back");
           }  else {
               // does user own the quote?
            if(foundQuote.author.id.equals(req.user._id)) {
                next();
            } else {
                req.flash("error", "You don't have permission to do that");
                res.redirect("back");
            }
           }
        });
    } else {
        req.flash("error", "You need to be logged in to do that");
        res.redirect("back");
    }
}

middlewareObj.checkCommentOwnership = function(req, res, next) {
 if(req.isAuthenticated()){
        Comment.findById(req.params.comment_id, function(err, foundComment){
           if(err){
               res.redirect("back");
           }  else {
               // does user own the comment?
            if(foundComment.author.id.equals(req.user._id)) {
                next();
            } else {
                req.flash("error", "You don't have permission to do that");
                res.redirect("back");
            }
           }
        });
    } else {
        req.flash("error", "You need to be logged in to do that");
        res.redirect("back");
    }
}

middlewareObj.isLoggedIn = function(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    console.log("could not authenticate");
    req.flash("error", "You need to be logged in to do that");
    res.redirect("/login");
}
// TODO: check the username and password against database
middlewareObj.isAuthenticatedFromPlugin = function(req, res, next){
    var username = req.body.username;
    var password = req.body.password;
    return next();
}

module.exports = middlewareObj;