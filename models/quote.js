var mongoose = require("mongoose");

var quoteSchema = new mongoose.Schema({
   name: String,
   price: String,
   imageUrl: String,
   webUrl: String,
   //image: { data: Buffer, contentType: String },
   description: String,
   author: {
      id: {
         type: mongoose.Schema.Types.ObjectId,
         ref: "User"
      },
      username: String
   },
   comments: [
      {
         type: mongoose.Schema.Types.ObjectId,
         ref: "Comment"
      }
   ]
});

module.exports = mongoose.model("Quote", quoteSchema);