var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");

var UserSchema = new mongoose.Schema({
    username: {type: String, match: /\S+@\S+\.\S+/},
    password: String,
    userType: String, // customer or purchasing agent
        address: String, // required only if type is customer
        agent: {type: [String], match: /\S+@\S+\.\S+/}, // required only if type is customer. If paid, up to 5 agents. Else, only 1 agent
    paid: Boolean,
        expiration: Date // not requierd if paid is false 
});

UserSchema.plugin(passportLocalMongoose)

module.exports = mongoose.model("User", UserSchema);