var express = require("express");
var router  = express.Router();
var passport = require("passport");
var User = require("../models/user");
var bodyParser = require('body-parser');
var middleware = require("../middleware");

router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true }));

//root route
router.get("/", function(req, res){
    res.render("landing");
});

// show register form
router.get("/register", function(req, res){
   res.render("register"); 
});

//handle sign up logic
router.post("/register", function(req, res){
    console.log("request received:");
    console.log(req.body.username);
    console.log(req.body.password);
    var newUser = new User({username: req.body.username});
    User.register(newUser, req.body.password, function(err, user){
        if(err){
            req.flash("error", err.message);
            return res.render("register");
        }
        passport.authenticate("local")(req, res, function(){
           req.flash("success", "Welcome to OneMore.Today " + user.username);
           res.redirect("/profile"); 
        });
    });
});

//show login form
router.get("/login", function(req, res){
   res.render("login"); 
});

// handling login logic
router.post("/login", passport.authenticate("local", 
    {
        failureRedirect: "/login"
    }), function(req, res){
        // console.log("login request received:");
        // console.log(req.body.username);
        // console.log(req.body.password);
        res.redirect("/quotes");
});

router.post("/loginFromPlugin", function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    console.log("login request received:");
    console.log(req.body.username);
    console.log(req.body.password);
    if (err) {
        console.log("1");
        return next(err); // will generate a 500 error
    }
    // Generate a JSON response reflecting authentication status
    if (! user) {
        console.log("2");
        return res.send("authentication failed");
    }
    // ***********************************************************************
    // "Note that when using a custom callback, it becomes the application's
    // responsibility to establish a session (by calling req.login()) and send
    // a response."
    // Source: http://passportjs.org/docs
    // ***********************************************************************
    req.login(user, function(err){
      if(err){
        console.log("3");
        return next(err);
      }
        console.log("4");
        return res.send("authentication succeeded");
    });      
  })(req, res, next);
});

// show contact page
router.get("/contact", function(req, res){
   res.render("contact"); 
});

// show introduction page
router.get("/intro", function(req, res){
   res.render("intro"); 
});

router.get("/profile", middleware.isLoggedIn, function(req, res){
   res.render("profile");
});

router.post("/profile", function(req, res){
    console.log("update profile request received:");
    // console.log(currentUser.username);
    // console.log(currentUser.password);
    console.log(req.body.userType);
    console.log(req.body.address);
    console.log(req.body.agentUsername);
    console.log(req.body.password);
    // console.log(req.body.paid);
    // console.log(req.body.expiration);
    
    var query = {'username':req.user.username};
    // req.newData.username = req.user.username;
    var updatedUser = {"$set": {address: req.body.address, userType: req.body.userType, agent: req.body.agentUsername}};
    var options = {new: true, upsert:true};//TODO: is upsert necessary here?
    User.findOneAndUpdate(query, updatedUser, options, function(err, doc){
        if (err) {
            req.flash("error", err.message);
        } else {
            req.flash("success", "Update successfully");
            res.redirect("/profile");// res.redirect(307, '/profile') will redirect to POST, by default it redirects to GET
        }
    });
    //TODO: Move this to the POST method for route /password
    if(req.body.password.trim()) {
        User.findByUsername(req.user.username).then(function(sanitizedUser) {
            if (sanitizedUser) {
                sanitizedUser.setPassword(req.body.password, function() {
                    sanitizedUser.save();
                    console.log('password reset successful');
                });
            } else {
                console.log('user does not exist');
            }
        }, function(err) {
            console.error(err);
        });
    }
});

// //TODO show reset password form
// router.get("/password", function(req, res){
//   res.render("password"); 
// });

// //TODO handle reset password  logic
// router.post("/password", function(req, res){
//     console.log("update profile request received:");
//     console.log(req.body.username);
//     console.log(req.body.password);
//     var newUser = new User({username: req.body.username});
//     User.register(newUser, req.body.password, function(err, user){
//         if(err){
//             req.flash("error", err.message);
//             return res.render("register");
//         }
//         passport.authenticate("local")(req, res, function(){
//           req.flash("success", "Welcome to OneMore.Today " + user.username);
//           res.redirect("/quotes"); 
//         });
//     });
// });

// logout route
router.get("/logout", function(req, res){
   req.logout();
   req.flash("success", "Logged out");
   res.redirect("/quotes");
});

module.exports = router;