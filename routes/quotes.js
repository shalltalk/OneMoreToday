var express = require("express");
var router = express.Router();
var Quote = require("../models/quote");
var User = require("../models/user");
var middleware = require("../middleware");
var request = require('request');
var PriceFinder = require('price-finder');
var cors = require('cors');
router.use(cors({
    origin: 'null'
}));
var token = "Something that should not be here lol";
var priceFinder = new PriceFinder();

//INDEX - show all quotes
router.get("/", function(req, res) {
    // Get all quotes from DB
    Quote.find({}, function(err, allQuotes) {
        if (err) {
            console.log(err);
        }
        else {
            res.render("quotes/index", {
                quotes: allQuotes
            });
        }
    });
});

//CREATE - add new quote to DB
router.post("/", middleware.isLoggedIn, function(req, res) {
    // get data from form and add to quotes array
    var name = req.body.name;
    var price = req.body.price;
    var imageUrl = req.body.imageUrl;
    var webUrl = req.body.webUrl;
    var desc = req.body.description;
    console.log("user._id :" + req.user._id);
    console.log("user.username :" + req.user.username);
    var author = {
        id: req.user._id,
        username: req.user.username
    }
    var newQuote = {
            name: name,
            price: price,
            imageUrl: imageUrl,
            webUrl: webUrl,
            description: desc,
            author: author
        }
        // Create a new quote and save to DB
    Quote.create(newQuote, function(err, newlyCreated) {
        if (err) {
            console.log(err);
        }
        else {
            //redirect back to quotes page
            // console.log(newlyCreated);
            res.redirect("/quotes");
        }
    });
});

//CREATE - add new quote to DB from plugin
router.post("/plugin/", middleware.isAuthenticatedFromPlugin, function(req, res) {
    // req is sent from plugin (do it in a similar way as the login function in popup.js)
    // imageData is in req (from the screenshot in plugin), 
    // upload will call createURL if success, 
    // which will then call createQuote
    //console.log(JSON.stringify(req));
    console.log(req.body.webUrl);
    upload(req, res);
});

//NEW - show form to create new quote
router.get("/new", middleware.isLoggedIn, function(req, res) {
    res.render("quotes/new");
});

// SHOW - shows more info about one quote
router.get("/:id", function(req, res) {
    //find the quote with provided ID
    Quote.findById(req.params.id).populate("comments").exec(function(err, foundQuote) {
        if (err) {
            console.log(err);
        }
        else {
            // console.log(foundQuote)
            //render show template with that quote
            res.render("quotes/show", {
                quote: foundQuote
            });
        }
    });
});

// EDIT CAMPGROUND ROUTE
router.get("/:id/edit", middleware.checkQuoteOwnership, function(req, res) {
    Quote.findById(req.params.id, function(err, foundQuote) {
        res.render("quotes/edit", {
            quote: foundQuote
        });
    });
});

// UPDATE CAMPGROUND ROUTE
router.put("/:id", middleware.checkQuoteOwnership, function(req, res) {
    // find and update the correct quote
    Quote.findByIdAndUpdate(req.params.id, req.body.quote, function(err, updatedQuote) {
        if (err) {
            res.redirect("/quotes");
        }
        else {
            //redirect somewhere(show page)
            res.redirect("/quotes/" + req.params.id);
        }
    });
});

// DESTROY CAMPGROUND ROUTE
router.delete("/:id", middleware.checkQuoteOwnership, function(req, res) {
    Quote.findByIdAndRemove(req.params.id, function(err) {
        if (err) {
            res.redirect("/quotes");
        }
        else {
            res.redirect("/quotes");
        }
    });
});

// ===========================create quotes from plugin===========================
function upload(req, response) {
    var imageData = req.body.imageData;
    var imageBuffer = Buffer.from(imageData.split('data:image/png;base64,').join(''), 'base64');
    var username = req.body.username; // TODO: check if this is good
    var filePath = "/" + username + "/" + getCurrentDateTime() + ".png";
    request({
        headers: {
            'Authorization': "Bearer " + token,
            'Content-Type': 'application/octet-stream',
            'Dropbox-API-Arg': JSON.stringify({
                path: filePath,
                mode: "add",
                autorename: true,
                mute: true
            })
        },
        uri: 'https://content.dropboxapi.com/2/files/upload',
        body: imageBuffer,
        method: 'POST'
    }, function(err, res, body) {
        if (err) {
            console.log(err);
        }
        else {
            console.log("success, calling createURL()");
            createURL(filePath, req); // req is the original request from plugin
        }
    });
}
// filePath should be passed into, the filePath should be generated in the upload method based on username.
function createURL(filePath, reqFromPlugin) {
    var data = {
        path: filePath,
    };
    request({
        headers: {
            'Authorization': "Bearer " + token,
            'Content-Type': 'application/json'
        },
        uri: 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
        body: JSON.stringify(data),
        method: 'POST'
    }, function(err, res, body) {
        if (err) {
            console.log(err);
            return;
        }
        else {
            console.log("calling getExistingURL()");
            if (res.statusCode == 200) {
                var sharedURL = replaceTrailingZero(JSON.parse(body).url);
                console.log("sharedURL is : " + sharedURL);
                createQuote(sharedURL, reqFromPlugin);
            }
            else if (res.statusCode == 409) {
                console.log("shared url already exist for " + filePath + ", trying to get existing shared url...");
                return getExistingURL(filePath, reqFromPlugin);
            }
            else {
                console.log("error in createURL");
                return;
            }
        }
    });
}

function replaceTrailingZero(url) {
    var newURL = url.substring(0, url.length - 1);
    return newURL + "1";
}

function getExistingURL(filePath, reqFromPlugin) {
    var data = {
        path: filePath,
    }

    request({
        headers: {
            'Authorization': "Bearer " + token,
            'Content-Type': 'application/json'
        },
        uri: 'https://api.dropboxapi.com/2/sharing/list_shared_links',
        body: JSON.stringify(data),
        method: 'POST'
    }, function(err, res, body) {
        if (err) {
            console.log(err);
            return;
        }
        if (res.statusCode == 200) {
            console.log("calling getExistingURL()");
            var sharedURL = replaceTrailingZero(JSON.parse(body).url);
            console.log("sharedURL is : " + sharedURL);
            createQuote(sharedURL, reqFromPlugin);
        }
        else {
            console.log("error in getExistingURL");
            return;
        }
    });
}

function createQuote(imageUrl, req) {
    // hard code name and price first, then use find-price to do it

    var webUrl = req.body.webUrl;
    priceFinder.findItemDetails(webUrl, function(err, itemDetails) {
        var name;
        var price;
        if (err) {
            console.log("err getting price from url: " + err);
            name = "";
            price = "";
        }
        name = itemDetails.name;
        price = itemDetails.price;
        var desc = req.body.description;
        var author;
        User.findOne({
            username: req.body.username
        }).exec(function(err, user) {
            author = {
                id: user._id,
                username: user.username
            }
            var newQuote = {
                name: name,
                price: price,
                imageUrl: imageUrl,
                webUrl: webUrl,
                description: desc,
                author: author
            }
            Quote.create(newQuote, function(err, newlyCreated) {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log("quote created success");
                }
            });
        })
    });
}

function getCurrentDateTime() {
    // replace T and : with a -, then throw away anything after .
    // this is UTC, basically PST + 7 hours
    var datetime = new Date().toISOString().replace(/T/, '-').replace(/:/g, '-').replace(/\..+/, '');
    console.log(datetime);
    return datetime;
}
// ===========================create quotes from plugin end=======================

module.exports = router;
