# Dynamic Price Feature - Yelp Camp

## Steps
- Add price to quote model as a String datatype
- Add price to views/quotes/new.ejs and views/quotes/edit.ejs (new and edit forms)
- Add price to views/camprounds/show.ejs (quote show page)